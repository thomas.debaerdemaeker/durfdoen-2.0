---
titel: International Students of History Association
id: isha
naam: International Students of History Association
verkorte_naam: International Students of History Association
konvent: ik
contact: Ghent.Isha@gmail.com
website: https://ishaghent.weebly.com/
social: 
  - platform: facebook
    link: https://www.facebook.com/ishaghent/
themas:
  -  internationaal
  -  cultuur
---

International Students of History Association

ISHA Ghent is one of more than 100 local sections of the International Students of History Association. Our goal is to unite local and international student by their love for the past.
ISHA Ghent organises events which focus on contact between international and local students. Our activities are mostly of an academic nature, but there is always room for a drink and a laugh. 
